# Dockerized Ubiquiti UniFi Management (v5)
## Description
This is a Docker container has been configured to install the latest version of UniFi (v5).
This container can be upgraded via standard apt tooling. This container is based on Ubuntu 16.04 base. 

## Release Notes
2016-08-31:

* Initial release!
* Based on Ubuntu 16.04 base image.
* Ubiquiti UniFi apt source tracks 'unifi5' repository.

## Installation and First Run
```bash
$ docker run --name unifiv5 -d \
             -p8080:8080 -p8081:8081 -p8443:8443 -p8843:8843 -p8880:8880 -p3478:3478 \
             --rm=false djmeier/docker-unifiv5 
```
The container is also built to expose the data volumes in the event that you would like to manipulate them
via local file system.  This is most useful for debug or backup situations.  The exposed volumes are as follows:

* /usr/lib/unifi/data
* /usr/lib/unifi/logs
* /var/log/supervisor 

## Stopping and Starting 
```bash
$ docker stop unifiv5
$ docker start unifiv5 
```
These commands will subsquently stop and start the UniFi container as such.  As long as the container is not
destroyed (rm/rmi) data will persist.

## Upgrading
Upgrading in container environments must be done with caution!  While the container will maintain data persistence
there is no good restoration path other than to reinstall the container, upgrade to the current release and then
reintroduce the data set prior to starting the UniFi services.  You have been warned, containrs may not be the
right solution for running UniFi in a large production environment!

To initialize a command line upgrade to UniFi (rolling upgrades) make sure the conainer is first running and then
issue the following commands.
```bash
$ docker exec -it unifiv5 bash
root@123456789abc:/usr/lib/unifi# apt-get update && apt-get -y upgrade
```
When the command has finished exit the exec shell as normal by typing 'exit'.
